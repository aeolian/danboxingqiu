<?php 

//如在使用中有任何疑问，请联系管理员解决。
//-----------------------请修改以下配置------------------------------------

//防盗链域名，多个用|隔开，（不设置盗链请留空，请填写授权是顶级域名的二级域名）
define('REFERER_URL', ''); //如：define('REFERER_URL', 'www.chaojibinbin.com|jx.chaojibinbin.com|chaojibinbin.com');

//此处设置防盗信息及错误提示
define('ERROR', '<html><meta name="robots" content="noarchive"><head><title>智能云解析</title></head><style>h1{color:#C7636C; text-align:center; font-family: Microsoft Jhenghei;}p{font-size: 1.2rem;text-align:center;font-family: Microsoft Jhenghei;}</style><body><table width="100%" height="100%" align="center"><td align="center"><h1>本站已开启API接口防盗</h1><p>如需使用，请联系管理员进行授权</p></table></body></html>');
//此处进行用户相关配置
$user = array(
    
        'uuid' => '016b2116-d68a-46da-80a1-f40fbc274eae', //请保持默认，无需修改
    
        'username' => 'youke', //请保持默认，无需修改
        
	    'domain' => '', //请设置您的解析域名，防止客户端被二开盗用数据，不要加端口号以及路径
		
	'tiaodomain' => '', //请设置客户端被二开盗用后跳转的地址，被盗用后直接跳转到您设置的地址。

		'hdd' => '3', //视频默认清晰度，1标清，2高清，3超清，4原画，如果没有高清会自动下降一级（请保持默认，无需修改）

		'autoplay' => '1', //电脑端autoplay是否自动播放：参数设置为：1,表示自动播放;参数设置为：0,表示不自动播放

		'h5' => '0', //手机端h5是否自动播放：参数设置为：1,表示自动播放;参数设置为：0,表示不自动播放

		'online' => '0', //当前无法解析的地址是否启动备用解析接口  默认开启,1:开启,0:关闭  开启时要在下面填入备用解析接口
  
        'ather' => '', //备用接口设置  //填写实例：'ather' => '',

        'dplayer' => 'bingo,http://chaojibinbin.com', //用户设置dplayer播放器右键,不设置请留空。填写实例:'dplayer' => ''
		
        'title' => 'bingo', //设置解析页面title名称   例如：'title' => 'bingo',

        'tongji' => '', //用户统计代码.  例如:s4.cnzz.com/z_stat.php?id=xxxxx&web_id=xxxxx,百度统计与之类似,多个代码请用逗号分开

        'ad' => '', //用户设置广告代码,例如: http://xxx.com/xxx.js,多个广告请用逗号分开 https://chaojibinbin.com/tv9ad3.js
        
        'cklogo' => '', //ckplayer播放器右上角logo标志,请填写完整地址.例如:https://chaojibinbin/img/logo.png ,不用请留空
        
        'ckhand' => 'bingo', //ckplayer播放器右键菜单
        
        'ckhref' => '', //ckplayer播放器右键菜单跳转链接
       

) 			
//-----------------------修改区域结束---------------------------------------
?>